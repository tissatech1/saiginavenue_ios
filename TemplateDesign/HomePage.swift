//
//  HomePage.swift
//  TemplateDesign
//
//  Created by TISSA Technology on 1/20/21.
//

import UIKit
import SideMenu
import SafariServices
import WebKit
import ProgressHUD

class HomePage: UIViewController,WKNavigationDelegate {
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var homepagetabbar: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        homepagetabbar.layer.shadowColor = UIColor.gray.cgColor
        homepagetabbar.layer.shadowOpacity = 1
        homepagetabbar.layer.shadowOffset = .zero
        homepagetabbar.layer.shadowRadius = 5
        homepagetabbar.clipsToBounds = false;
        homepagetabbar.layer.masksToBounds = false;

        let defaults = UserDefaults.standard
        let restid = defaults.integer(forKey: "clickedStoreId")
        GlobalClass.restaurantGlobalid = String(restid)
       
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        homepagetabbar.layer.cornerRadius = 10
        
        webView.navigationDelegate = self
        webView.backgroundColor = .clear
        
        var linkshow = String()
        if GlobalClass.restaurantGlobalid == "142" {
            linkshow = "https://order.profitboss.com/restaurant/saigonavenue-monrovia"
        }else{
            linkshow = "https://order.profitboss.com/restaurant/saigonavenue-westcovina"
        }
        
        let url = URL(string: linkshow)!
    webView.load(URLRequest(url: url))
    webView.allowsBackForwardNavigationGestures = true
        webView.scrollView.isScrollEnabled = true
       self.webView.addObserver(self, forKeyPath: #keyPath(WKWebView.isLoading), options: .new, context: nil)
        
    }
    
    @IBAction func reload(_ sender: UIButton) {

      //  webView.reload()
        
        if(self.webView.canGoBack) {
                self.webView.goBack()
               }

    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
            if keyPath == "loading" {
                if webView.isLoading {
                    ProgressHUD.show()
                    
                } else {
                    
                    ProgressHUD.dismiss()
                }
            }
        }
    
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("End loading")
//        webView.evaluateJavaScript("document.getElementsByTagName(div).remove();", completionHandler: { (response, error) -> Void in
//                           })
        ProgressHUD.dismiss()
    }

        func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
            ProgressHUD.show()
           // print(webView.url!)
        }

        func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {

            print("error  - \(error)")
        }


    
   
    //MARK: - tab bar button actions

//    @IBAction func locationClicked(_ sender: UIButton) {
//
//        let home = self.storyboard?.instantiateViewController(withIdentifier: "RestaurantListPage") as! RestaurantListPage
//        self.navigationController?.pushViewController(home, animated: true)
//
//    }
   
    @IBAction func cartClicked(_ sender: UIButton) {
        
        let home = self.storyboard?.instantiateViewController(withIdentifier: "RestaurantListPage") as! RestaurantListPage
               self.navigationController?.pushViewController(home, animated: true)
            
    }
    
    @IBAction func orderClicked(_ sender: Any) {
        
        let order = self.storyboard?.instantiateViewController(withIdentifier: "AboutusPage") as! AboutusPage
        self.navigationController?.pushViewController(order, animated: true)
        
    }
    
    @IBAction func moreClicked(_ sender: Any) {
        
        let order = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsPage") as! ContactUsPage
        self.navigationController?.pushViewController(order, animated: true)
        
    }
    
    
    
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        ProgressHUD.dismiss()

                                        
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        
                                        ProgressHUD.dismiss()

                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                     
                                        
                                       
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
    
}

extension HomePage: SideMenuNavigationControllerDelegate {
    
    func sideMenuWillAppear(menu: SideMenuNavigationController, animated: Bool) {
       // print("SideMenu Appearing! (animated: \(animated))")
        self.view.alpha = 0.5
    }
    
    func sideMenuDidAppear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Appeared! (animated: \(animated))")
        
        self.view.alpha = 0.5
    }
    
    func sideMenuWillDisappear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Disappearing! (animated: \(animated))")
        
        self.view.alpha = 1
        
       // self.performSegue(withIdentifier: "login", sender: self)
    }
    
    
    func sideMenuDidDisappear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Disappeared! (animated: \(animated))")
        
        self.view.alpha = 1
        
      
    }
   
}
