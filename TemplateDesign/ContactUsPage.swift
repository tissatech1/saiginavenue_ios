//
//  ContactUsPage.swift
//  TemplateDesign
//
//  Created by TISSA Technology on 1/29/21.
//

import UIKit
import Alamofire

class ContactUsPage: UIViewController {
    @IBOutlet weak var phTF: UILabel!
    @IBOutlet weak var emailTF: UILabel!
    @IBOutlet weak var addressTF: UILabel!
    @IBOutlet weak var timeTF: UILabel!
    @IBOutlet weak var locationtitle: UILabel!
    @IBOutlet weak var secondphTF: UILabel!
    @IBOutlet weak var secondemailTF: UILabel!
    @IBOutlet weak var secondaddressTF: UILabel!
    @IBOutlet weak var secondtimeTF: UILabel!
    @IBOutlet weak var secondlocationtitle: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        let defaults = UserDefaults.standard
        let restid = defaults.integer(forKey: "clickedStoreId")
        GlobalClass.restaurantGlobalid = String(restid)
        
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        adminlogincheck()
    }
    

    @IBAction func backBtnClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: false)

    }
    
    //MARK: Admin Login
    
    func adminlogincheck(){

        let urlString = GlobalClass.DevlopmentApi + "rest-auth/login/v1/"

        AF.request(urlString, method: .post, parameters: ["username":GlobalClass.adminusername, "password":GlobalClass.adminpassword,"restaurant_id":"1"],encoding: JSONEncoding.default, headers: nil).responseJSON {
       response in
         switch response.result {
                       case .success:
                           print(response)

                           if response.response?.statusCode == 200{
                            
                            let dict :NSDictionary = response.value! as! NSDictionary
                            // print(dict)
                            
                            let tok = dict.value(forKey: "token")
                            
                            let defaults = UserDefaults.standard
                            
                            defaults.set(tok, forKey: "adminToken")
                            self.getrestaurantApi()
                           // self.GetCategoryList()
                          
                           }else{
                            
                            if response.response?.statusCode == 401{
                                
                                ERProgressHud.sharedInstance.hide()
                               // self.sessionAlert()
                                
                            }else if response.response?.statusCode == 500{
                                
                                ERProgressHud.sharedInstance.hide()

                                 let dict :NSDictionary = response.value! as! NSDictionary
                                
                                self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                
                               //  print(dict.value(forKey: "msg") as! String)
                            }else{
                                
                                self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                               }
                          
                           }
                           
                           break
                       case .failure(let error):
                        
                        ERProgressHud.sharedInstance.hide()
                        print(error.localizedDescription)
                        
                        let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                        
                        
                        let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                        
                        let msgrs = "URLSessionTask failed with error: The request timed out."
                        
                        
                        if error.localizedDescription == msg {
                            
                            self.showSimpleAlert(messagess:"No internet connection")
                            
                        }else if error.localizedDescription == msgr ||  error.localizedDescription == msgrs {
                            
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                        
                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                        }

                           print(error)
                       }
       }


    }
    
    func getrestaurantApi(){
        
        var admintoken = String()
        let defaults = UserDefaults.standard
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
            admintoken = (defaults.object(forKey: "adminToken")as? String)!
        }else{
            admintoken = (defaults.object(forKey: "custToken")as? String)!
        }
        
        let autho = "token \(admintoken)"
        
        let urlString = GlobalClass.DevlopmentApi+"restaurant/?group_name=saigon"

           
        print(" categoryurl - \(urlString)")
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho
            ]
      

        AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
              switch response.result {
                            case .success:
                                print(response)

                                if response.response?.statusCode == 200{
                                    
                                    let dict :NSDictionary = response.value! as! NSDictionary
                                   
                                    let arraylist :NSArray  = dict.value(forKey: "results") as! NSArray
                                    
                                    let dictObj = arraylist[0] as! NSDictionary

                                                                       let dataPH  = dictObj["phone"]as? String
                                                                        let dataEmail  = dictObj["email"]as? String
                                                                        let dataADD  = dictObj["address"]as! String
                                                                        let dataCITY  = dictObj["city"]as! String
                                                                        let dataState  = dictObj["state"]as! String
                                                                        let dataZip  = dictObj["zip"]as! String
                                                                        let dataTime  = dictObj["working_hours"]as? String
                                    
                                    self.locationtitle.text! = dataCITY ?? " "
                                                                        self.phTF.text! = dataPH ?? " "
                                                                        self.emailTF.text! = dataEmail ?? " "
                                                                        self.addressTF.text! = "\(dataADD) \(dataCITY), \(dataState) \(dataZip)"
                                    
                                                                        self.timeTF.text! = dataTime ?? " "
                                    
                                    let dictObj1 = arraylist[1] as! NSDictionary

                                                                       let dataPH1  = dictObj1["phone"]as? String
                                                                        let dataEmail1  = dictObj1["email"]as? String
                                                                        let dataADD1  = dictObj1["address"]as! String
                                                                        let dataCITY1  = dictObj1["city"]as! String
                                                                        let dataState1  = dictObj1["state"]as! String
                                                                        let dataZip1  = dictObj1["zip"]as! String
                                                                        let dataTime1  = dictObj1["working_hours"]as? String
                                    
                                    self.secondlocationtitle.text! = dataCITY1;                                                                       self.secondphTF.text! = dataPH1 ?? " "
                                                                        self.secondemailTF.text! = dataEmail1 ?? " "
                                                                        self.secondaddressTF.text! = "\(dataADD1) \(dataCITY1), \(dataState1) \(dataZip1)"
                                    
                                                                        self.secondtimeTF.text! = dataTime1 ?? " "
                                    
                                    
                                    print(dict)
                                    
                                    ERProgressHud.sharedInstance.hide()

                                    
                                }else{
                                    
                  if response.response?.statusCode == 401{
                                    
                    ERProgressHud.sharedInstance.hide()

                    self.SessionAlert()
                          
                                    
                                    }
                                    
                                    
                                    if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {

                            self.showSimpleAlert(messagess:"No internet connection")

                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{

                            self.showSimpleAlert(messagess:"Slow Internet Detected")

                                }else{
                                
                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                       

                                        
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        
                                      

                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                      
                                        
                                        
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    

}
